 #ifndef GAMEOBJECT_H
#define GAMEOBJECT_H

#include <QObject>
#include <QString>
#include <QStringList>
#include <QDateTime>
#include "board.h"

class GameObject : public QObject
{
    Q_OBJECT
    
    Q_PROPERTY(int columns MEMBER columns NOTIFY gameLoaded)
    Q_PROPERTY(int rows MEMBER rows NOTIFY gameLoaded)
    Q_PROPERTY(int nBombs MEMBER nBombs NOTIFY gameLoaded)
    Q_PROPERTY(QString difficulty MEMBER difficulty NOTIFY gameLoaded)
    
    Q_PROPERTY(int toReveal READ getToReveal NOTIFY updated)
    Q_PROPERTY(int nFlags READ getNFlags NOTIFY updated)
    Q_PROPERTY(QStringList images MEMBER images NOTIFY updated)
    Q_PROPERTY(bool isWon MEMBER isWon NOTIFY updated)
    Q_PROPERTY(bool isLost MEMBER isLost NOTIFY updated)
    Q_PROPERTY(QString statusImage MEMBER statusImage NOTIFY updated)
    Q_PROPERTY(bool flagMode MEMBER flagMode  NOTIFY updated)
    
    Q_PROPERTY(qint64 timer MEMBER timer NOTIFY timerUpdated())

public:
    explicit GameObject(QObject *parent = nullptr);
    
public slots:
    void toggleFlagMode();
    void registerClicked(int index);
    void registerHeld(int index);
    void newGame(int new_cols, int new_rows, int new_nBombs);
    void getTimer();
    
signals:
    void gameLoaded();
    void gameStarted();
    void gameWon();
    void gameLost();
    void updated();
    void timerUpdated();

private:
    int columns;
    int rows;
    int nElements;
    int nBombs;
    QString difficulty;
    
    bool flagMode;
    bool isWon;
    bool isLost;
    bool isStarted;
    
    int getToReveal();
    int getNFlags();
    
    GameBoard *board;
    
    QStringList images;
    void updateImages();
    QString statusImage;
    
    qint64 timer;
    QDateTime startTime;
    
    void flagged(int index);
    void uncovered(int index);
};

#endif // GAMEOBJECT_H
