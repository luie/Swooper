#include "board.h"

GameBoard::GameBoard(int c, int r, float nb) {
    // init board overall, create fields
    columns=c;
    rows=r;
    nElements = columns*rows;
    nBombs=nb;
    nFlags=0;
    fields = new Field[nElements];
    
    isLost = false;
    isWon = false;
    
    toReveal = nElements-nBombs;
    
    // place bombs on board
    int bombs_placed = nBombs;
    int choice;
    srand(time(NULL)); // init random number gen
    while(bombs_placed!=0){
        choice = std::rand() % nElements;
        if (!fields[choice].isBomb){
            fields[choice].isBomb=true;
            bombs_placed--;
        }
    }
    
    // populate # of neighbouring bombs for every field 
    setNnBombs();
}

GameBoard::~GameBoard() {
    delete[] fields;
}

void GameBoard::setNnBombs() {
    int col,row;
    for (int i = 0; i<nElements;i++) {
        col=getCol(i);
        row=getRow(i);
        if (fields[i].isBomb)
            for (int c=col-1;c<=col+1;c++)
                for (int r=row-1;r<=row+1;r++)
                    if (!(c==col&&r==row) && isValid(c,r))
                        fields[getIndex(c,r)].nnBombs++;
    }
}

void GameBoard::flag(int index) {
    if (!(fields[index].isBomb && !fields[index].isCovered)){
        if (fields[index].isFlag){
            fields[index].isFlag = false;
            nFlags--;
        }else{
            fields[index].isFlag = true;
            nFlags++;
        }
    }
    if (toReveal==0 && nFlags==nBombs)
            isWon=true;
}

void GameBoard::uncover(int index) {    
    //if uncovered and not a bomb or empty
    if (!fields[index].isCovered && !isLost && !isWon){
        int col=getCol(index);
        int row=getRow(index);
        int nnFlags=0;
        // get No. of surrounding flags
        for (int c=col-1;c<=col+1;c++)
            for (int r=row-1;r<=row+1;r++)
                if (!(c==col && r==row) && isValid(c,r) && fields[getIndex(c,r)].isFlag)
                    nnFlags++;
        // reveal non flagged surrounding fields if #flags == #bombs
        if (nnFlags==fields[index].nnBombs)
            for (int c=col-1;c<=col+1;c++)
                for (int r=row-1;r<=row+1;r++)
                    if (!(c==col && r==row) && isValid(c,r) && !fields[getIndex(c,r)].isFlag && fields[getIndex(c,r)].isCovered)
                        uncover(getIndex(c,r));
    }
    
    // if not already uncovered, uncover
    if (fields[index].isCovered && !fields[index].isFlag && !isLost && !isWon){
        fields[index].isCovered = false;
        toReveal--;
        // if no bombs around, uncover surrounding fields as well
        if (fields[index].nnBombs==0 && !fields[index].isBomb){
            int col=getCol(index);
            int row=getRow(index);
            for (int c=col-1;c<=col+1;c++)
                for (int r=row-1;r<=row+1;r++)
                    if (!(c==col && r==row) && isValid(c,r) && fields[getIndex(c,r)].isCovered)
                        uncover(getIndex(c,r));
        }
    }
    
    // game ending conditions
    if (fields[index].isBomb && !fields[index].isCovered)
        isLost=true;
    if (toReveal==0 && nFlags==nBombs)
            isWon=true;
}

bool GameBoard::isValid(int column, int row) {
    return (0<=column && column<columns && 0<=row && row<rows);
}

int GameBoard::getCol(int index) {
    return index % columns;
}

int GameBoard::getRow(int index) {
    return floor(index / columns);
}

int GameBoard::getIndex(int column, int row) {
    return (row*columns)+column;
}

