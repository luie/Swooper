// Adapted from https://invent.kde.org/nickre/kirigamiclock
//
#ifndef SCOREMODEL_H
#define SCOREMODEL_H

#include <string>
#include <iostream>
#include <sstream>
#include <fstream>
#include <unistd.h>
#include <QAbstractListModel>
#include <QStandardPaths>
#include <QDir>

class Score : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString name READ getName WRITE setName)
    Q_PROPERTY(QString type READ getType WRITE setType)
    Q_PROPERTY(int time READ getTime WRITE setTime)
    Q_PROPERTY(QString image READ getImage WRITE setImage)
    
public:
    explicit Score(QObject* parent = nullptr);

    QString getName() const { return name; }
    QString getType() const { return type; }
    int getTime() const { return time; }
    QString getImage() const { return image; }
    void setName(QString name) { this->name = name; }
    void setType(QString type) { this->type = type; }
    void setTime(int time) { this->time = time; }
    void setImage(QString image) { this->image = image; }

private:
    QString name;
    QString type;
    int time;
    QString image;
};

class ScoreModel : public QAbstractListModel
{
    Q_OBJECT
    Q_PROPERTY(QString lastUser MEMBER lastUser NOTIFY userUpdated)
    
public:
    explicit ScoreModel(QObject *parent = nullptr);

    enum {
        NameRole = Qt::DisplayRole,
        TypeRole = Qt::UserRole + 0,
        TimeRole = Qt::UserRole + 1,
        ImageRole = Qt::UserRole + 2
    };

    int rowCount(const QModelIndex & parent) const override;
    QVariant data(const QModelIndex & index, int role) const override;
    bool setData(const QModelIndex & index, const QVariant & value, int role) override;
    Qt::ItemFlags flags(const QModelIndex & index) const override;
    QHash<int, QByteArray> roleNames() const override;
    
public slots:
    void addScore(QString inname,QString intype,int intime);
    bool isHighscore(QString intype,int intime);
    void saveScores();
    
signals:
    void userUpdated();

private:
    QList<Score*> mList;
    QString lastUser;
};

#endif // SCOREMODEL_H
