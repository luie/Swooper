import QtQuick 2.6
import org.kde.kirigami 2.4 as Kirigami
import QtQuick.Controls 2.1 as Controls
import QtQuick.Layouts 1.2

Kirigami.ScrollablePage{
    title: "Settings"
    implicitWidth: applicationWindow().width
    Kirigami.FormLayout {
            width: page.width

            Controls.GroupBox {
            title: "Difficulty"
            Layout.alignment: Qt.AlignHCenter

            ColumnLayout {
                id: options
                Controls.RadioButton {
                    id:easy
                    text: "Easy"
                    checked: true
                }
                Controls.RadioButton {
                    id:normal
                    text: "Normal"
                    checked: false
                }
                Controls.RadioButton {
                    id:hard
                    text: "Hard"
                    checked: false
                }
                Controls.RadioButton {
                    id:custom
                    text: "Custom"
                    checked: false
                }
                Text{
                    text: "Columns:"
                    visible: (custom.checked) ? true : false
                }
                Controls.SpinBox {
                    id:colschoose
                    editable: true
                    visible: (custom.checked) ? true : false
                    from: 0
                    value: 16
                    to: 30
                    stepSize: 1

                    validator: DoubleValidator {
                        bottom: Math.min(colschoose.from, colschoose.to)
                        top:  Math.max(colschoose.from, colschoose.to)
                    }
                }
                
                Text{
                    text: "Rows:"
                    visible: (custom.checked) ? true : false
                }
                Controls.SpinBox {
                    id:rowschoose
                    editable: true
                    visible: (custom.checked) ? true : false
                    from: 0
                    value: 16
                    to: 30
                    stepSize: 1

                    validator: DoubleValidator {
                        bottom: Math.min(rowschoose.from, rowschoose.to)
                        top:  Math.max(rowschoose.from, rowschoose.to)
                    }
                }
                
                Text{
                    text: "Bombs:"
                    visible: (custom.checked) ? true : false
                }
                Controls.SpinBox {
                    id:bombschoose
                    editable: true
                    visible: (custom.checked) ? true : false
                    from: 0
                    value: 40
                    to: rowschoose.value * colschoose.value
                    stepSize: 1

                    validator: DoubleValidator {
                        bottom: Math.min(bombschoose.from, bombschoose.to)
                        top:  Math.max(bombschoose.from, bombschoose.to)
                    }
                }
            }
        }
        
            Controls.Button {
                text: "New Game"
                anchors.horizontalCenter: parent.horizontalCenter
                enabled: true
                onClicked: {
                    if (easy.checked){
                        gameObject.newGame(8,8,10)
                    }else if (normal.checked){
                        gameObject.newGame(16,16,40)
                    }else if (hard.checked){
                        gameObject.newGame(16,30,99)
                    }else if (custom.checked){
                        gameObject.newGame(colschoose.value,rowschoose.value,bombschoose.value)
                    }
                    switchToPage("board")
                }
            }
        }    
}
