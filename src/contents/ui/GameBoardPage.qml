import QtQuick 2.12
import org.kde.kirigami 2.4 as Kirigami
import QtQuick.Controls 2.1
import QtQuick.Layouts 1.12

Kirigami.Page {
    title:"Swoop"
    implicitWidth: applicationWindow().width
    
    // ************* GameBoard Helpers **************//    
    Timer {
        id: timer
        property string text: "---s"
        interval: 500; running: true; repeat: true
        triggeredOnStart: true
        onTriggered: gameObject.getTimer()
    }
    
    Connections {
        target:gameObject
        onGameWon: {
            if (scoreModel.isHighscore(gameObject.difficulty,gameObject.timer))
                highscoreDialog.open()
        }
    }
    
    // ************* Header **************//
    header:ToolBar {
        height: 35
        RowLayout {
            anchors.horizontalCenter: parent.horizontalCenter
            Text {
                text: gameObject.nFlags+(" / ")+gameObject.nBombs
            }
            ToolSeparator {}
            Image {
                id:gameStatus
                height:parent.height*0.8
                source: gameObject.statusImage
                MouseArea{
                    anchors.fill: parent
                    onClicked: gameObject.newGame(gameObject.columns,gameObject.rows,gameObject.nBombs)
                }
            }
            ToolSeparator {}
            Image {
                id:flagMode
                height:parent.height*0.8
                source: (gameObject.flagMode) ? "qrc:///bomb.png" : "qrc:///flag.png"
                MouseArea{
                    anchors.fill: parent
                    onClicked: gameObject.toggleFlagMode()
                }
            }
            ToolSeparator {}
            Text {
                id: time
                text: (gameObject.timer > 999) ? ">999s" : " " + gameObject.timer + "s"
            }
        }
    }

    // ************* The Grid **************//
    Flickable {
        id:flick
        //anchors.top:parent.top
        contentWidth: grid.width; contentHeight: grid.height
        width: parent.width*0.9; height: (parent.height*0.9)
        Grid {            
            id: grid
            columns: gameObject.columns // presets? easy/normal/hard
            rows: gameObject.rows
            rowSpacing: 0
            columnSpacing: 0
            Repeater {
                id: reptr
                model: gameObject.images;
                delegate: field
            }
        }
    }
    
    // ************* The Fields **************//
    Component {
        id: field
        Rectangle {
            property int gridx: index % grid.columns
            property int gridy: Math.floor(index / grid.columns);
            width: 30
            height: 30
            color: Qt.rgba(Math.random() * (1 - 0.24) + 0.24,Math.random() * (1 - 0.6) + 0.6,Math.random()  * (1 - 0.85) + 0.85,0.15)
            Image {
                width:parent.width
                height:parent.height
                anchors.centerIn: parent
                source: modelData
                MouseArea {
                    anchors.fill: parent
                    pressAndHoldInterval: 400
                    enabled: !(gameObject.isWon || gameObject.isLost)
                    onClicked: gameObject.registerClicked(index)
                    onPressAndHold:gameObject.registerHeld(index)
                }
            }
        }
    }
}
