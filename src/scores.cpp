// Adapted from https://invent.kde.org/nickre/kirigamiclock
//
#include "scores.h"

Score::Score(QObject *parent) : QObject(parent) {
    name = "User";
    type = "Easy";
    time = 1000;
}

ScoreModel::ScoreModel(QObject *parent) : QAbstractListModel (parent) {
    
    QString path =  QStandardPaths::writableLocation(QStandardPaths::AppDataLocation);
    QDir dir(path);
    if (!dir.exists())
        dir.mkpath(dir.absolutePath());
    std::string name = path.toStdString() + "/highscores.sav";
    std::cout <<path.toStdString()<<std::endl << name<<std::endl;
    if(access( name.c_str(), F_OK ) == -1) {
        std::ofstream outfile(name,std::ofstream::trunc);
        if(outfile){
            outfile << "User;Easy;1000;qrc:///flag_green.png;\nUser;Normal;1000;qrc:///flag_yellow.png;\nUser;Hard;1000;qrc:///flag.png;\nUser;Custom;1000;qrc:///flag_pink.png;" << std::endl;
            outfile.close();
        }
    }
    
    std::ifstream infile(name);
    std::string line;std::string data;
    std::string rname = "User";std::string rtype;std::string rtime;std::string rimage;
    while(std::getline(infile, line)) {
        std::stringstream linestream(line);
        std::getline(linestream, rname, ';');
        std::getline(linestream, rtype, ';');
        std::getline(linestream, rtime, ';');
        std::getline(linestream, rimage, ';');
        
        Score *addscr = new Score(this);
        addscr->setName(QString::fromStdString(rname));
        addscr->setType(QString::fromStdString(rtype));
        addscr->setTime(std::stoi(rtime));
        addscr->setImage(QString::fromStdString(rimage));
        
        beginInsertRows(QModelIndex(), rowCount(QModelIndex()), rowCount(QModelIndex()));
        mList.append(addscr);
        endInsertRows();
    }
    this->lastUser = QString::fromStdString(rname);
}

int ScoreModel::rowCount(const QModelIndex & parent) const {
    Q_UNUSED(parent);
    return mList.size();
}

QVariant ScoreModel::data(const QModelIndex & index, int role) const {
    switch (role) {
    case Qt::DisplayRole:
        return mList.at(index.row())->getName();
    case TypeRole:
        return mList.at(index.row())->getType();
    case TimeRole:
        return mList.at(index.row())->getTime();
    case ImageRole:
        return mList.at(index.row())->getImage();
    default:
        return QVariant();
    }
}

bool ScoreModel::setData(const QModelIndex &index, const QVariant &value, int role) {
    if (index.isValid()) {
        switch (role) {
        case NameRole:
            if (value.type() == QVariant::String) {
                mList[index.row()]->setName(value.toString());
                emit dataChanged(index, index, QVector<int> { NameRole });
                return true;
            }
            return false;
        case TypeRole:
            if(value.type() == QVariant::String) {
                mList[index.row()]->setType(value.toString());
                emit dataChanged(index, index, QVector<int> { TypeRole });
                return true;
            }
            return false;
        case TimeRole:
            if(value.type() == QVariant::Int) {
                mList[index.row()]->setTime(value.toInt());
                emit dataChanged(index, index, QVector<int> { TimeRole });
                return true;
            }
            return false;
        case ImageRole:
            if(value.type() == QVariant::String) {
                mList[index.row()]->setImage(value.toString());
                emit dataChanged(index, index, QVector<int> { ImageRole });
                return true;
            }
            return false;
        }
    }
    return false;
}

Qt::ItemFlags ScoreModel::flags(const QModelIndex &index) const {
    Q_UNUSED(index);
    return Qt::ItemIsEditable;
}

QHash<int, QByteArray> ScoreModel::roleNames() const {
    auto roles = QHash<int, QByteArray>();
    roles[NameRole] = "name";
    roles[TypeRole] = "type";
    roles[TimeRole] = "time";
    roles[ImageRole] = "image";
    return roles;
}

void ScoreModel::addScore(QString inname,QString intype,int intime) {    
    for (int i = 0; i < mList.size(); ++i){
        if (mList.at(i)->getType() == intype && mList.at(i)->getTime()>intime){
            this->setData(createIndex(i,NameRole), inname, NameRole);
            this->setData(createIndex(i,TypeRole), intype, TypeRole);
            this->setData(createIndex(i,TimeRole), intime, TimeRole);
            break;
        }
    }
    this->lastUser = inname;
}

bool ScoreModel::isHighscore(QString intype,int intime) {    
    for (int i = 0; i < mList.size(); ++i)
        if (mList.at(i)->getType() == intype && mList.at(i)->getTime()>intime)
            return true;
    return false;
}


void ScoreModel::saveScores() {
    std::string name = QStandardPaths::writableLocation(QStandardPaths::AppDataLocation).toStdString() + "/highscores.sav";
    std::ofstream outfile(name,std::ofstream::trunc);
    if(outfile)
        for (int i = 0; i < mList.size(); ++i)
            outfile << mList.at(i)->getName().toStdString() << ";" << mList.at(i)->getType().toStdString() << ";" << mList.at(i)->getTime() << ";"  << mList.at(i)->getImage().toStdString() << ";"  << std::endl;
    outfile.close();
}


	

